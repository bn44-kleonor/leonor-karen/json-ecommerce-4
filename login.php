<?php 
 require "partials/header.php";

 var_dump($_SESSION);
?>

<div class="container">
	<!-- LOGIN FORM -->
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">
					<form method="POST" action="assets/lib/processLogin.php">
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"
					    name="email">
					    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					  </div>
					  <div class="form-group">
					    <label for="exampleInputPassword1">Password</label>
					    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"
					    name="password">
					  </div>
					  
					  <button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
 require "partials/footer.php";
?>